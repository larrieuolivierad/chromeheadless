import asyncio
import json
from pathlib import Path
from pyppeteer import launch, page
from .decorators import func_log


MIME_TYPES = {
    "pdf": "application/pdf",
    "png": "image/png"
}

class PDFPaperFormats():
    LETTER = "letter"
    LEGAL = "legal"
    TABLOID = "tabloid"
    LEDGER = "ledger"
    A0 = "A0"
    A1 = "A1"
    A2 = "A2"
    A3 = "A3"
    A4 = "A4"
    A5 = "A5"


# pylint: disable=dangerous-default-value
class BaseRenderer():
    """
    Simple wrapper around pyppeteer.
    You do not need to worry about asyncio initialisation, user should not now than
    asyncio is used.
    Allow you to get the following outputs from a chromeheadless browser:
        - pdf
        - pdg
    samples:
        # from file
            template = open(file_path, 'r').read()
            browser_page = BaseRenderer(page_content=template)
            image = browser_page.get_png(wait=2)
            pdf = browser_page.get_pdf(wait=2)

        # from url
            browser_page = BaseRenderer(url="http://someurl.org")
            image = browser_page.get_png(wait=2)
            pdf = browser_page.get_pdf(wait=2)
    """

    def __init__(self, url=None, page_content=None):
        # basic instance initialisation
        self.url = url
        self.page_content = page_content
        self.instance = None
        self.page = None
        self._window_variables = {}

    def add_window_variable(self, key, value):
        self._window_variables[key] = value

    @func_log
    async def init(self):
        # initialize a chrome page
        self.instance = await launch(
            args=['--no-sandbox'],
            headless=True,
            handleSIGINT=False,
            handleSIGTERM=False,
            handleSIGHUP=False,
        )
        self.page = await self.instance.newPage()

    @func_log
    def get_renderer(self, output_format):
        renderer = None
        if output_format == "pdf":
            renderer = getattr(self.page, 'pdf')
        if output_format == "png":
            renderer = getattr(self.page, 'screenshot')
        return renderer

    # pylint: disable=no-self-use
    @func_log
    def get_renderer_options(self, output_format, output_filename, pdf_format=None, width=None, height=None, margin={}, landscape=False):
        # page output options
        options = {"fullPage": True}
        if output_filename:
            options = {**options, 'path': output_filename + "." + output_format}
        if width and height:
            options = {**options, "width": width, "height": height}
        if pdf_format:
            options = {**options, "format": pdf_format}
        if output_format == "pdf":
            options = {**options, "margin": margin, "printBackground": True, "landscape": landscape}

        return options

    @func_log
    async def render(self, output_filename="", output_format="", margin={}, wait=0, wait_options=[],
                     pdf_format=None, width=None, height=None, landscape=False):
        # page initialisation
        await self.init()

        # get the appropriate renderer, pdf or image (png)
        renderer = self.get_renderer(output_format=output_format)

        # page output options
        renderer_options = self.get_renderer_options(
            output_format=output_format,
            output_filename=output_filename,
            pdf_format=pdf_format,
            width=width,
            height=height,
            margin=margin,
            landscape=landscape
        )

        # should we set viewport
        if isinstance(width, (float, int)) and isinstance(height, (float, int)):
            dimensions = {"width": width, "height": height}
            await self.page.setViewport(dimensions)

        # add javascritp window variables
        if self._window_variables:
            for key, value in self._window_variables.items():
                func = '() => {{ window.{key}={value} }}'.format(key=key, value=value)
                if self.url is not None:
                    await self.page.evaluateOnNewDocument(func)
                else:
                    await self.page.evaluate(func)

        # set the page content, url or string content, url accept file://
        if self.url is not None:
            await self.page.goto(self.url, {"waitUntil": wait_options})
        else:
            await self.page.setContent(self.page_content)

        # wait x seconds if needeed
        if wait:
            await asyncio.sleep(wait)

        # render the page
        output = await renderer(renderer_options)

        # close the page
        await self.close()

        # return the result
        return output

    @func_log
    async def close(self):
        await self.page.close()
        await self.instance.close()

    # pylint: disable=assignment-from-no-return
    @func_log
    def _run(self, options):
        # start asyncio loop and wrap to self.render
        loop = asyncio.new_event_loop()
        loop = asyncio.set_event_loop(loop)
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(self.render(**options))

    @func_log
    def render_png(self, output_filename=None, wait_options=["load", ],
                   width=800, height=600, wait=0):
        """
        Shortcuts to get a png from the page content.
        """
        options = {
            "output_filename": output_filename,
            "output_format": "png",
            "wait": wait,
            "wait_options": wait_options,
            "width": width,
            "height": height,
        }
        return self._run(options), MIME_TYPES.get("png")

    @func_log
    def render_pdf(self, output_filename=None, wait_options=["load", ],
                   pdf_format=PDFPaperFormats.A4, margin={}, wait=0, landscape=False, width=None, height=None):
        """
        Shortcuts to get a pdf from the page content.
        width and heigh are promgrammatically calculated.
        margin support px, in, cm and mm.
        """

        if width and height:
            pdf_format = None

        options = {
            "output_filename": output_filename,
            "output_format": "pdf",
            "wait": wait,
            "wait_options": wait_options,
            "width": width,
            "height": height,
            "pdf_format": pdf_format,
            "landscape": landscape,
            "margin": {
                "top": margin.get('top', 0),
                "left": margin.get('left', 0),
                "bottom": margin.get('bottom', 0),
                "right": margin.get('right', 0),
            }
        }

        return self._run(options), MIME_TYPES.get("pdf")


class HighChartsRenderer(BaseRenderer):
    """
    HighCharts renderer
    """

    def __init__(self, data={}):
        super().__init__()
        file_path = Path(__file__).parent / 'html/highcharts.html'
        self.url = file_path.as_uri()
        # disable animations
        data['chart'] = {"animations": False}
        data['plotOptions'] = {"series": { "animation": False } }
        self.add_window_variable("HIGHCHARTDATA", json.dumps(data))
