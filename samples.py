import os
from chrome_renderers import BaseRenderer, HighChartsRenderer, PDFPaperFormats

# from url
renderer = BaseRenderer(url="http://google.fr")
# only get bytes
# if width and height are not provided the default dimensions are width=800, height=600
# the dimensions will affect the browser page viewport so for pdf renderer you ll need to
# specify the page format.
# the default pdf page format is A4, so be sure to set the viewport for your desired rendering.
google_png = renderer.render_png(wait=2)
google_pdf = renderer.render_pdf(wait=2, pdf_format=PDFPaperFormats.A4)

# get bytes and create output_filename, with custom dimensions for each renderer
google_png = renderer.render_png(output_filename="google", wait=2, width=1600, height=800)
google_pdf = renderer.render_pdf(output_filename="google", wait=2, pdf_format=PDFPaperFormats.A4)

# from string is the same, just the page_content parameter is changing in constructor
page_content = """
<!doctype html>
<html lang="en">
<head>

<style>

    body {
        background: #2bcbba;
        color: #fed330;
    }

</style>

</head>

<body>
    <h1 id="hello"></h1>
    <h3 id="another"></h3>
</body>

<script>
    var hello = document.getElementById('hello')
    hello.innerText = window.HELLO_WORLD

    var another = document.getElementById('another')
    another.innerText = window.ANOTHER_VARIABLE
</script>

</html>
"""
renderer = BaseRenderer(page_content=page_content)
# you can set some variables in the javascript window object, for use in you content scripts
# here you ll have access to window.HELLO_WORL and window.ANOTHER_VARIABLE
# from the page_content javascript code.
renderer.add_window_variable("HELLO_WORLD", "'YOUR GREAT CONTENT HELLO WORLD'")
renderer.add_window_variable("ANOTHER_VARIABLE", "'YOU HAVE GENERATED A DYNAMIC DOCUMENT'")

hello_world_png = renderer.render_png(output_filename="hello_world", wait=2, width=1600, height=800)
hello_world_pdf = renderer.render_pdf(output_filename="hello_world", wait=2, pdf_format=PDFPaperFormats.A4)
hello_world_pdf = renderer.render_pdf(output_filename="hello_world_landscape", wait=2, pdf_format=PDFPaperFormats.A4, landscape=True)
# highcharts
data = {
      "title": {
        "text": "Fruit Consumption",
      },
      "xAxis": {
        "categories": [
          "Apples",
          "Bananas",
          "Oranges",
          "Pineapples",
          "Blueberries",
        ],
      },
      "yAxis": {
        "title": {
          "text": "Fruit eaten",
        },
      },
      "chart": {
        "type": "line",
      },
      "series": [
        {
          "name": "Jane",
          "data": [1, 0, 4, 0, 3],
        },
        {
          "name": "John",
          "data": [5, 7, 3, 2, 4],
        },
        {
          "name": "Doe",
          "data": [0, 0, 0, 1, 0],
        },
        {
          "name": "Oliver",
          "data": [3, 8, 2, 7, 5],
        },
      ],
    }

renderer = HighChartsRenderer(data=data)
renderer.render_png(output_filename="highcharts")

paper_formats = [ k for k in PDFPaperFormats.__dict__ if not "__" in k]

for pf in paper_formats:
    renderer.render_pdf(pdf_format=pf ,output_filename="highcharts_{}".format(pf), margin={"left": 200, "right": 250})
    renderer.render_pdf(pdf_format=pf ,output_filename="highcharts_landscape_{}".format(pf), margin={"left": 50, "right": 50}, landscape=True)
# you can create your own renderer
# a visit card render for the sample

class VisitCardRenderer(BaseRenderer):
    html = """
    <!doctype html>
    <html lang="en">
    <head>

    <style>

        body {
            background: #000;
            color: #C0C0C0;
        }
        .content {
            background: #8e44ad;
        }
    </style>

    </head>
    <body>

        <div class="content">
        <span id="lastname"></span> <span id="firstname"></span>
        <ul>
        <li id="address">Address: </li>
        <li id="postal_code">Postal Code: </li>
        <li id="phone">Phone: </li>
        </ul>
        </div>
    </body>

    <script>

        var firstname = document.getElementById('firstname')
        var lastname = document.getElementById('lastname')
        var address = document.getElementById('address')
        var postal_code = document.getElementById('postal_code')
        var phone = document.getElementById('phone')

        firstname.innerText = window.FIRSTNAME
        lastname.innerText = window.LASTNAME
        address.innerText = window.ADDRESS
        postal_code.innerText = window.POSTAL_CODE
        phone.innerText = window.PHONE

    </script>

    </html>
    """
    def __init__(self, name, lastname, address, postal_code, phone):
        super().__init__()
        self.page_content = self.html
        self.add_window_variable("FIRSTNAME", '"{}"'.format(name))
        self.add_window_variable("LASTNAME", '"{}"'.format(lastname))
        self.add_window_variable("ADDRESS", '"{}"'.format(address))
        self.add_window_variable("POSTAL_CODE", '"{}"'.format(postal_code))
        self.add_window_variable("PHONE", '"{}"'.format(phone))


visit_card_renderer = VisitCardRenderer("larrieu", "olivier", "toulouse", 31000, "06 76 50 86 12")
visit_card_renderer.render_png(output_filename="visit_card")
visit_card_renderer.render_pdf(output_filename="visit_card", width="40cm", height="20cm", margin={"left": "10cm", "right": "10cm"})
visit_card_renderer.render_pdf(output_filename="visit_card_landscape", landscape=True)
